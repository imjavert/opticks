#pragma once

#include "X4_API_EXPORT.hh"
#include "geomdefs.hh"

struct X4_API X4geomdefs
{
    static const char* kOutside_ ; 
    static const char* kSurface_ ; 
    static const char* kInside_ ; 
    static const char* EInside_( EInside in ); 
};


