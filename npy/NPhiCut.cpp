#include <iostream>
#include <cmath>
#include <cassert>
#include <cstring>

#include "NGLMExt.hpp"
#include "nmat4triple.hpp"

// sysrap-
#include "OpticksCSG.h"

// npy-
#include "NPhiCut.hpp"
#include "NBBox.hpp"
#include "Nuv.hpp"
#include "NPlane.hpp"

#include "PLOG.hh"


void nphicut::check() const 
{

}

float nphicut::operator()(float x_, float y_, float z_) const 
{
  glm::vec4 p(x_,y_,z_,1.f);  //position vector
    if(gtransform) p = gtransform->v * p ;  

    // v:inverse-transform, for gtransform non null

    glm::vec3 c = center(); 
    float r = radius();
    
    

    //float d_sphere = glm::distance( glm::vec3(p), c ) - r ;
    //used for CSG intersect in 
    
    //float sd = fmaxf( d_sphere, d_slab );  
    // CSG intersect of sphere with slab

    return 0.f; //complement ? -sd : sd ; 
    } 


nbbox nphicut::bbox() const 
{ //generates bounding box
    glm::vec3 c = center(); 
    float r = radius(); 
    //gets centre and radius

    glm::vec3 mx(c.x + xmax(), c.y + ymax(), c.z + r);
    glm::vec3 mi(c.x + xmin(), c.y + ymin(), c.z - r);
    //set minmax dimensions, determined by phi angle

    nbbox bb = make_bbox(mi, mx, complement); //create bounding box

    return gtransform ? bb.make_transformed(gtransform->t) : bb ; 
    //if global transform above this, return bounding box transformed by this
}

glm::vec3 nphicut::gseedcenter() const 
{
    glm::vec3 c = center(); 
    glm::vec4 seedcenter( c.x, c.y, c.z, 1.f );
    //glm::vec4 seedcenter( xcenter(), ycenter(), c.z, 1.f ); 
    // not sure what does yet, assume for global transform application

    return apply_gtransform(seedcenter);
}

glm::vec3 nphicut::gseeddir() const 
{
  glm::vec4 dir(0,0,0,0); 
  //glm::vec4 dir(1,1,0,0); 
  // z=1 for nzsphere, not sure what does exactly. 

    return apply_gtransform(dir);
}



void nphicut::pdump(const char* msg) const 
{
    std::cout 
              << std::setw(10) << msg 
              << " label " << ( label ? label : "-" )
              << " center " << center()
              << " radius " << radius()
	      << " xmin " << xmin()
              << " xmax " << xmax()
	      << " xcentre " << xcentre()
	      << " ymin " << ymin()
              << " ymax " << ymax()
	      << " ycentre " << ycentre()
              << " startPhi " << startPhi()
	      << " deltaPhi " << deltaPhi()
              << " endPhi " << endPhi()
              << " gseedcenter " << gseedcenter()
              << " gtransform " << !!gtransform 
              << std::endl ; 

    if(verbosity > 1 && gtransform) std::cout << *gtransform << std::endl ;
}


int nphicut::par_euler() const 
{
   return 2 ; 
}

unsigned nphicut::par_nvertices(unsigned /*nu*/, unsigned /*nv*/) const 
{
   return 0 ;     
}

glm::vec3 nphicut::par_pos_model(const nuv& uv) const 
{
  unsigned s  = uv.s(); 
    assert(s == 0 || s == 1);
    //number of surface

    //float r_ = radius(); //originally for phisphere, now defunct?

    glm::vec3 pos(0,0,0);
    pos.x = x();
    pos.y = y();
    pos.z = z();
    // start on transform

    //float kludge = 0.1f ; 
   // avoid NOpenMesh complex edge problem by leaving a gap

   unsigned surfaces = par_nsurf();

    if( surfaces == 2)
      { 

        assert( s == 0 || s== 1 );
	    //check valid surface index

        if(s == 0)
	  { 

	    //for startPhi plane
        }
        else
	  {

	    //for endPhi plane
        }
    } 
    else
      { //if hemisphere
        assert( s == 0 ); 
	//for startPhi plane

        

	}
  
  return pos ;
}
