#pragma once

#include <cassert>

#include "NNode.hpp"
#include "NQuad.hpp"
#include "NGLM.hpp"

struct nbbox ; 

#include "NPY_API_EXPORT.hh"

struct NPY_API nphicut : nnode {

    float operator()(float x, float y, float z) const ;

    nbbox bbox() const ; 

    glm::vec3 gseedcenter() const  ;
    glm::vec3 gseeddir() const ;

    void pdump(const char* msg="nphicut::pdump") const ;

    unsigned  par_nsurf() const ; 
    glm::vec3 par_pos_model(const nuv& uv) const  ;
    int       par_euler() const ; 
    unsigned  par_nvertices(unsigned nu, unsigned nv) const ; 

    unsigned flags() const ;

    float     x() const ; 
    float     y() const ; 
    float     z() const ; 
    float     radius() const  ; 
    //ret position and radius

    float  startPhi() const ; 
    float  endPhi() const ; 
    float  deltaPhi() const ;
    //ret phi angles 

    float     xmin() const ; 
    float     xmax() const ;
    float     xcenter() const ;

    float     ymin() const ;
    float     ymax() const ;
    float     ycenter() const ;
    //ret min, max, center for x, y

    glm::vec3 center() const  ; 
    //ret center point

    void   check() const ; 

    
};


inline NPY_API glm::vec3 nphicut::center() const { return glm::vec3(x(),y(),z()) ;  } 

inline NPY_API float nphicut::x() const {      return param.f.x ; }
inline NPY_API float nphicut::y() const {      return param.f.y ; }
inline NPY_API float nphicut::z() const {      return param.f.z ; }
inline NPY_API float nphicut::radius() const { return param.f.w ; }


inline NPY_API unsigned nphicut::flags() const { return param2.u.x ; }


inline float NPY_API nphicut::startPhi() const { return param1.f.x; }
inline float NPY_API nphicut::endPhi() const { return param1.f.y;  }
inline float NPY_API nphicut::deltaPhi() const { return endTheta() - startTheta(); }


inline NPY_API float nphicut::xmax() const {
 
  float xdist = radius();

  float endPhi = endPhi();
  float startPhi = startPhi();
  //get angles

  if(endPhi >= 0 && startPhi <= 0) return xdist; 
  //requires check to void missing interlude

  endPhi = cos(endPhi());
  startPhi = cos(startPhi());
  //set angles to cos of angles

  xdist *= endPhi > startPhi ? 
    endPhi : startPhi;
  //multiply by larger value 

  return xdist; 
  //return max
}


inline NPY_API float nphicut::xmin() const {  float xdist = radius();

  float endPhi = cos(endPhi());
  float startPhi = cos(startPhi());
  float xdist = radius();

  xdist *= endPhi < startPhi ? 
    endPhi : startPhi;
  //multiply by smaller value 

  return xdist;
  //return min

}


inline NPY_API float nphicut::xcentre() const {      
  return 0.5* ( xmin() + xmax()) ; //give midpoint of both
}


inline NPY_API float nphicut::ymax() const {

  float endPhi = endPhi();
  float startPhi = startPhi();
  //get angles

  if(endPhi >= 90 && startPhi <= 90) return ydist; 
  //check to avoid missing interlude

  endPhi = sin(endPhi());
  startPhi = sin(startPhi());
  //set angles to sin of angles

  ydist *= endPhi > startPhi ? 
    endPhi : startPhi;
  //multiply by larger value 

  return ydist;
}

inline NPY_API float nphicut::ymin() const {  float xdist = radius();

  float endPhi = endPhi();
  float startPhi = startPhi();
  //get angles

  if(endPhi >= -90 && startPhi <=-90) return ydist; 
  //check to avoid missing interlude

  endPhi = sin(endPhi());
  startPhi = sin(startPhi());
  //set angles to sin of angles

  ydist *= endPhi < startPhi ? 
    endPhi : startPhi;
  //multiply by smaller value 

  return ydist;

}

inline NPY_API float nphicut::ycentre() const {      
  return 0.5 * ( ymin() + ymax()) ; 
  //give midpoint of both
}



inline NPY_API void init_phicut(nphicut* s, const nquad& param, const nquad& param1, const nquad& param2)
{
    s->param = param ; 
    s->param1 = param1 ; 
    s->param2 = param2 ; 
    //sets nphicut params

    s->check();

}

inline NPY_API nphicut* make_phicut(const nquad& param, const nquad& param1, const nquad& param2)
{
    nphicut* n = new nphicut ; 

    nnode::Init(n, CSG_PHICUT); 
    //gives basic nphicut node, CSG type 
    //NNode left/right default to NULL (used for comparisons)

    init_phicut(n, param, param1, param2 );
    //initialises phicut with params

    return n ; 
}

inline NPY_API nphicut* make_phicut(float x, float y, float z, float radius, float startPhi, float endPhi )
{
    nquad p0, p1, p2 ; 

    p0.f = {x,y,z,radius} ; //basic info
    p1.f = {startPhi, endPhi, 0,0} ; //angle info

    p2.u = {3, 0, 0, 0} ;   
    //^^ fix to avoid uninitialized gibberish getting into partBuffer

    return make_phicut(p0, p1, p2);
}
